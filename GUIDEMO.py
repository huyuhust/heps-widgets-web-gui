import tables as tb
import numpy as np
import pandas as pd
import h5py
import ipysheet
import hdf5plugin
import matplotlib.pyplot as plt  
import ipywidgets as widgets
from IPython.display import display
from ipywidgets import Layout, Button, Box, FloatText, Textarea, Dropdown, Label, IntSlider , interactive
from exampleworkflow import *

def make_box_layout():
     return widgets.Layout(
        border='solid 1px black',
        margin='0px 10px 10px 0px',
        padding='5px 5px 5px 5px'
     )

def make_tab_layout():
     return widgets.Layout(
	width='100%', 
	height='450px', 
	border='solid 1px black',
	margin='0px 10px 10px 0px',
	padding='5px 5px 5px 5px'
)

def make_button_layout():
     return widgets.Layout(
	backgroundcolor = '#4CAF50',
	color =  'white'
)

class GUIDemo():
	def __init__(self):
		super().__init__()
		self.datanp = np.array([])
		self.x = np.array([])
		self.y = np.array([])    
		self.sheet = ipysheet.sheet(3,3)        

		loadex = self.make_loadex()
		control = self.make_control()

		self.button.on_click(self.on_button_clicked)

		wei1 =  widgets.interactive_output(self.f1, {'Median_Size': self.Median_Size})
		wei2 =  widgets.interactive_output(self.f2, {'Median_Size': self.Median_Size})
		plot_2D = widgets.Box(children = [wei1], layout = make_box_layout())
		#plot_hist = widgets.Box(children = [self.wei2])

		self.tab = widgets.Tab( layout = make_tab_layout())
		self.tab.set_title(0, 'Hist')
		self.tab.set_title(1, 'Table')
		self.tab.children = [wei2, self.sheet]
		sub_out = widgets.HBox(children = [loadex, control, plot_2D], layout = make_box_layout())
		self.widget = widgets.VBox(children =[Label('GUI demo'), sub_out, self.tab], layout = make_box_layout())


	def make_control(self):
		self.Median_Size = widgets.IntSlider(min=0,max=200,step=1,value=1,description='N:')
		directDist = widgets.IntSlider(min=0,max=200,step=1,value=169,description='directDist:')
		centerX = widgets.FloatSlider(min=0,max=2000,step=0.1,value=1049.967,description='centerX:')
		centerY = widgets.FloatSlider(min=0,max=2000,step=0.1,value=1049.967,description='centerY:')
		pixelX = widgets.IntSlider(min=0,max=200,step=1,value=75,description='pixelX:')
		pixelY = widgets.IntSlider(min=0,max=200,step=1,value=75,description='pixelY:')
		PlanRotation = widgets.FloatText(step=0.01,value=64.66877,description='PlanRot:')
		azi = widgets.FloatRangeSlider(value=[95, 137.5],min=0,max=360.0,step=0.1,description='azi:')
		rad = widgets.FloatRangeSlider(value=[10, 27.5],min=0,max=40,step=0.1,description='rad:')

		return widgets.VBox(children = [widgets.Label('Control:'), self.Median_Size, directDist, centerX, centerY, pixelX, pixelY, PlanRotation, azi, rad], layout = make_box_layout())
        

	def make_loadex(self):
		self.button = widgets.Button(description="execute!", button_style='primary', layout = make_button_layout())
		self.Dataitem = widgets.Textarea(value='', placeholder='Please click the button to excute!', layout={'height': '100%'}, disabled = False)
		return widgets.VBox(children = [self.button, Label('Data items:'), self.Dataitem], layout = make_box_layout())


	def f1(self, Median_Size):
		selem = int(Median_Size)
		fig1 = plt.figure(constrained_layout=True,figsize=(8, 6))
		ax_neu = fig1.add_subplot(111) 
		if self.datanp.shape[0] != 0 :
			im = ax_neu.imshow(self.datanp[selem], vmin=0, vmax=10, cmap="RdBu")
			plt.colorbar(im, ax=ax_neu,extend='both')
		plt.show()

	def f2(self, Median_Size):
		selem = int(Median_Size)
		#ax_hist.cla()
		fig2 = plt.figure(constrained_layout=True, figsize=(16, 6))
		ax_hist = fig2.add_subplot(111) 
		if self.y.shape[0] != 0 :
			line = ax_hist.plot(self.x, self.y[selem])
		plt.show()

        
	def on_button_clicked(self, b):
		wf = AIWorkflow('AzimuthalIntegrator')
		wf.setLogLevel(5)
		wf.initialize(workflow_engine='PyWorkflowEngine', workflow_environment = init_dict, algorithms_cfg = cfg_dict)
		wf.execute()
		datakeys = wf.data_keys()

		#datakeys = ['caldata', 'expdata', 'result']
		self.Dataitem.value = datakeys[0] + '\n' + datakeys[1] + '\n' + datakeys[2]

		#dataH5 = h5py.File('/home/huy/GUI/data/scan_00575_data_000001.h5','r')
		self.datanp =  wf.get_data('expdata')  # np.array(dataH5['entry']['data']['data'])
		#f = h5py.File('/home/tianhl/workarea/test_scan.h5','r')
		h = wf.get_data('result')  #f['/entry/result']
		self.x = np.array(h['x'])
		self.y = np.array(h['y'])    
		self.sheet = ipysheet.from_array(self.y)        
		self.tab.children = list(self.tab.children)[0:-1] + [self.sheet]


if __name__ == '__main__':
    a = GUIDemo()
    a.widget
